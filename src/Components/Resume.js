import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import Card from '@material-ui/core/Card';

const styles = theme => ({
  work: {
    margin: '10px 20px',
    padding: '30px 10px'
  },
});

class Resume extends Component {
  render() {
    const { classes } = this.props;

    if(this.props.data){
      var answer = this.props.data.answers.map(function(ans){
         return <div key={ans.title} className="row item">
            <div className="twelve columns">
               <h3>{ans.title}</h3>
               <p className="info">{ans.phrase} <span>|</span> <em className="date">{ans.phraseBy}</em></p>
               <p dangerouslySetInnerHTML={{__html: ans.description}}/>
            </div>
         </div>
       });

      var education = this.props.data.education.map(function(edu){
         return <div key={edu.school} className="row item">
            <div className="twelve columns">
               <h3>{edu.school}</h3>
               <p className="info">{edu.degree} <span>|</span> <em className="date">{edu.graduated}</em></p>
               <p>
                 {edu.description}
               </p>
            </div>
         </div>
       });

      var work = this.props.data.work.map(function(job){
         const imgUrl = 'images/work/' + job.image;
         return <div key={job.company} className="row item">
            <Card variant="outlined" className={classes.work}>
              <div className="three columns">
                <img src={imgUrl} alt="work"/>
              </div>
                <div className="nine columns">
                <h3>{job.company}</h3>
                <p className="info">{job.title}<span>|</span> <em className="date">{job.years}</em></p>
                <p dangerouslySetInnerHTML={{__html: job.description}}/>
              </div>
            </Card>
         </div>
       });

      var skills = this.props.data.skills.map(function(skill){
         var className = 'bar-expand '+skill.name.toLowerCase();
         return <li key={skill.name}><span style={{width:skill.level}} className={className}></span><em>{skill.name}</em></li>
       });
    }
    return (
      <section id="resume">
         <div className="row work">
            <div className="header-wrapper">
               <h1><span>Work</span></h1><br />
            </div>
              {work}
         </div>
         <div className="row education">
            <div className="header-wrapper">
               <h1><span>Education</span></h1><br />
            </div>
              {education}
         </div>
         <div className="row answer">
            <div className="header-wrapper">
               <h1><span>Answers</span></h1><br />
            </div>
              {answer}
         </div>
         <div className="row skill">
            <div className="three columns header-col" style={{paddingLeft: 0}}>
               <h1><span>Skills</span></h1>
            </div>
            <div className="nine columns main-col">
               <p>Certified Kubernetes Administrator 자격증 소지. 백엔드 어플리케이션을 만들고 운영합니다. 메인 스택은 Fastapi & Django. 입니다.
                   TOEIC 955. 번역이 안 된 책은 편하게 원서로 보곤 합니다. 관리자 페이지를 만들다 보니 Vue, React, Angular
                   를 모두 한 번 이상 경험하였습니다.
                 Git, Slack, Trello 등 협업을 위한 기술에 익숙합니다.
               </p>
              <div className="bars">
                <ul className="skills">
                  {skills}
                </ul>
              </div>
            </div>
          </div>
          {this.props.children}
      </section>
    );
  }
}

export default withStyles(styles)(Resume);
